import React, { Component, Fragment } from "react";

// import { Container } from './styles';
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";

import Register from "./register";
import "./login.css";
import Landpage from './authenticated/landpage'

import util from '../helpers/auth'

const auth = util.auth
const database = util.database

export default class Login extends Component {

  constructor(props){
    super(props)

    this.state = {
      email:'',
      password: '',
      authenticaded: false,
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.redirect.bind(this)
  }

  redirect(){
    if(auth.getCurrentUser){
      let prevState = this.state;
      this.setState({
        ...prevState,
        authenticaded: true
      })
    }
  }

  async handleSubmit(e){    
    var credentials = {
      email: "",
      password: "",      
    }
    credentials.email = this.state.email
    credentials.password = this.state.password
    e.preventDefault()
    await auth.signIn(credentials, (e) => {
      if(e){
        console.log(e)
      } else {
        this.redirect()
      }
    })
  }

  handleChange(e){    
    if (e.target.id === 'email'){      
      this.setState({
        email: e.target.value        
      })
    } else {      
      this.setState({
        ...this.state,
        password: e.target.value
      })
    }    
  }
  

  render() {

    if(this.state.authenticaded){
      return(
        <Fragment>
          <Route path='/landpage' component={Landpage}/>
          <Redirect to='/landpage'/>
        </Fragment>
      )
    }

    return (
      <div className="master">
        <div id="description">
          <span id="caderneta">
            <font color="white">Caderneta</font>
          </span>
          <span id="eletronica">
            <font color="white">Eletronica</font>
          </span>
          <span id="small-description">
            <font color="white">
              Organize seus debitos de maneira rapida e facil
            </font>
          </span>
        </div>
        <div className="foreground">
          <div id="form">
            <span id="login">Login</span>
            <div>
              <span id="reg-text">Não tem uma conta?</span>
              <Link to="/register" id="reg-link">
                {" "}
                Clique aqui!
              </Link>
            </div>
            <Route path="/register" component={Register} />
            <form id="fields">
              <div className="form-group">
                <input
                  type="email"
                  className="form-control"
                  id="email"
                  aria-describedby="emailHelp"
                  placeholder="E-mail"
                  onChange={this.handleChange}
                  required
                />
              </div>
              <div className="form-group">
                <input
                  type="password"
                  className="form-control"
                  id="exampleInputPassword1"
                  placeholder="Senha"
                  onChange = {this.handleChange}
                  required
                />
              </div>
              <div className="form-group" id="below">
                <div>
                  <input type="checkbox" id="rememberme" />
                  <label className="form-check-label" htmlFor="rememberme">
                    Lembrar de mim
                  </label>
                </div>
                <Link to="/recover" id="forgot">
                  Esqueci minha senha
                </Link>
              </div>
              <button type="submit" className="btn btn-primary" id="submit" onClick={this.handleSubmit}>
                Login
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
