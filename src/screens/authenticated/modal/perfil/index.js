import React, { Component } from 'react';

// import { Container } from './styles';

import {Modal, Button, Container, Alert, ModalHeader, ModalFooter, ModalBody, render } from "reactstrap"

const Perfil = (props) => {
  return(
    <>
    <Modal isOpen={props.display}>
      <ModalHeader toggle={() => props.callback()}>Perfil</ModalHeader>
      <ModalBody>
        <span id="reg-text">Nome *:</span>
        <input type="Nome"
          className="form-control"
          id="Nome"
          placeholder="Nome"></input>
        <span id="reg-text">Email *:</span>
        <input type="Email"
          className="form-control"
          id="Email"
          placeholder="Email"></input>
        <span id="reg-text">Senha Atual *:</span>
        <input type="Password"
          className="form-control"
          id="SenhaAtual"
          placeholder="Senha Atual"></input>
        <span id="reg-text">Nova Senha *:</span>
        <input type="Password"
          className="form-control"
          id="NovaSenha"
          placeholder="Nova Senha"></input>
        <span id="reg-text">Confirmar Nova Senha *:</span>
        <input type="Password"
          className="form-control"
          id="NovaSenha"
          placeholder="Confirmar Nova Senha"></input>
      </ModalBody>
      <ModalFooter> <Button color="primary">Salvar</Button>
        <Button color="secondary" onClick={() => props.callback()}>Cancel</Button>
      </ModalFooter></Modal>
  </>
  )
};

export default Perfil;

