import React, { Component } from 'react';

// import { Container } from './styles';
import { Modal, Button, Container, Alert, ModalHeader, ModalFooter, ModalBody, render } from "reactstrap"
// import { Container } from './styles';



let data = { }

function handleSubmit(callback){   
  callback(data)
}

function handleChange(e){  
  switch (e.target.id){
    case "Descricao":
      data = {
        ...data,
        descricao: e.target.value
      }
      break
    case "DatadeVencimento":
      data = {
        ...data,
        date: e.target.value
      }
      break;
    case "Debito":
      data = {
        ...data,
        tipo: e.target.value
      }
      break;
    case "Categoria":
      data = {
        ...data,
        categoria: e.target.value
      }
      break;
    case "Valor":
      data = {
        ...data,
        valor: e.target.value
      }
  }
}

const Debito = (props) => {  
  return (
    <>
      <Modal isOpen={props.display}>
        <ModalHeader toggle={() => {          
            props.callback()          
          }}
           >Débito</ModalHeader>
        <ModalBody>
          <span id="reg-text">Descrição*:</span>
          <input type="Descricao"
            className="form-control"
            id="Descricao"
            placeholder="Descrição"
            onChange={handleChange.bind(this)}></input>
          
          <span id="reg-text">Data de Vencimento*:</span>
          <input type="date"
            className="form-control"
            id="DatadeVencimento"
            placeholder="Data de Vencimento"
            onChange={handleChange.bind(this)}></input>

          <span id="reg-text">Tipo de Débito*:</span>
          <br></br>
          <select id="Debito"
            onChange={handleChange.bind(this)}>
              <option style={{display: 'none'}}></option>
              <option value="receber">À Receber</option>
              <option value="pagar">À Pagar</option>
          </select>
          <br></br>
          <span id="reg-text">Categoria*:</span>
          <input type="Categoria"
            className="form-control"
            id="Categoria"
            placeholder="Categoria"
            onChange={handleChange.bind(this)}></input>

          <span id="reg-text">Valor*:</span>
          <input type="Valor"
            className="form-control"
            id="Valor"
            placeholder="Valor"
            onChange={handleChange.bind(this)}></input>
        </ModalBody>
        <ModalFooter> 
          <Button color="primary" onClick={() => handleSubmit(props.callback)}>Salvar</Button>
          <Button color="secondary" onClick={() => props.callback()}>Cancel</Button>
        </ModalFooter>
      </Modal>
    </>
  )
}



export default Debito;
