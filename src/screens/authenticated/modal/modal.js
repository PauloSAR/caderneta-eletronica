import React, { Component } from 'react';

// import { Container } from './styles';

import Debito from './debito/index'
import Perfil from './perfil/index'
import Sobre from './sobre/index'

// import { Container } from './styles';

const modal = (props) => {  
  if(props.type === 'debito'){
    return <Debito display={true} callback={props.callback}/>
  } else if(props.type === 'perfil'){
    return <Perfil display={true} callback={props.callback}/>
  } else if(props.type === 'sobre'){
    return <Sobre display={true} callback={props.callback}/>
  } else{
    return (
      <>
        <Sobre display={false} callback={props.callback}/>
        <Perfil display={false} callback={props.callback}/>
        <Debito display={false} callback={props.callback}/>
      </>
    )
  }
}

export default modal;
