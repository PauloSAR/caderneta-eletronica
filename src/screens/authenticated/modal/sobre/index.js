import React, { Component } from 'react';

// import { Container } from './styles';

import {Modal, Button, Container, Alert, ModalHeader, ModalFooter, ModalBody, render } from "reactstrap"

const Sobre = (props) => {
  return(
    <>
    <Modal isOpen={props.display}>
      <ModalHeader toggle={() => props.callback()}>Sobre</ModalHeader>
      <ModalBody>
        <span id="reg-text"><h6>Caderneta Eletrônica:</h6></span>
        Caderneta Eletrônica é um sistema para ajudar o seu usuário à gerir seus débitos
        e ter o controle dele.
        <br/>
        <br/>
         
        <span id="reg-text"><h6>Legenda:</h6></span>
        Campos com "*" na frente são obrigatórios serem preenchidos!
      </ModalBody>
      <ModalFooter> <Button color="primary">Salvar</Button>
        <Button color="secondary" onClick={() => props.callback()}>Cancel</Button>
      </ModalFooter></Modal>
  </>
  )
};

export default Sobre;

