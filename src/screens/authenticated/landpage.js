import React, { Component, useRef, useState } from 'react';

import util from '../../helpers/auth'
import "./landpage.css";
import { Button, Container, Alert, ModalHeader, ModalFooter, ModalBody, render } from "reactstrap"
import MyModal from './modal/modal'
import List from '../../components/list'

const auth = util.auth
const db = util.database

export default class Landpage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      visible: true,      
      modalType: 'none',
      data: undefined,     
    }
    this.ref = db.firestore().collection('users')
    this.ref.where("email", "==", auth.getCurrentUser().email).get().then((snap) => {
      snap.forEach((doc) => {
        this.userRef = doc
      })
    })
    this.unsubscribe = false

    this.refreshData = this.refreshData.bind(this)
    this.printData = this.printData.bind(this)

    this.toggleModalDebito = this.toggleModalDebito.bind(this)    
    this.toggleModalPerfil = this.toggleModalPerfil.bind(this)
    this.toggleModalSobre = this.toggleModalSobre.bind(this)
    this.modalCallback = this.modalCallback.bind(this)
  }  

  refresh() {
    this.update()
  }

  update = () => {
    let data
    db.firestore().collection('users')
      .where("email", "==", auth.getCurrentUser().email).get().then((snap) => {
        snap.forEach((doc) => {                    
          data = doc.data()                    
          this.setState({            
            data: data
          }) 
        })
      })               
  }

  componentDidMount(){     
    this.update()
  }

  async refreshData() {
    let newdata = await db.getCurrentUserData();
    this.setState({
      userData: newdata
    })
  }
  async printData() {
    await this.refreshData()    
  }

  logout() {
    localStorage.clear();
    window.location.href = '/';
  }

  modalCallback(data){
    console.log(data)
    this.setState({
      ...this.state,
      modalType: 'none'
    })
    if(!this.userRef.data().debitos){
      let debArray = []
      debArray.push(data)
      this.userRef.ref.update({
        debitos: debArray
      }).then(() => {
        console.log("Added successfully")
        this.update()
    })
      .catch(e => console.log("ERROR: ", e))      
    } else{
      let debArray = [ ]
      debArray = this.userRef.data().debitos
      debArray.push(data)
      this.userRef.ref.update({
        debitos: debArray
      }).then(() => {
        console.log("Added successfully")
        this.update()
      })
      .catch(e => console.log("ERROR: ", e))
    }
  }

  toggleModalDebito = () => {
    this.setState({
      ...this.state,
      modalType: 'debito',
    });        
  }

  toggleModalPerfil = () => {
    this.setState({
      ...this.state,
      modalType: 'perfil'
    });    
  }

  toggleModalSobre = () => {
    this.setState({
      ...this.state,
      modalType: 'sobre'
    });    
  }

  render() {
    let lista    
    if(this.state.data != undefined){
      if(Array.isArray(this.state.data.debitos)){
        lista = <List items={this.state.data.debitos}/>
      } else if (typeof this.state.data.debitos != "undefined"){
        let pseudolist = []
        pseudolist.push(this.state.data.debitos)
        lista = <List items={pseudolist}/>
      } else {
        lista = <span>Carregando</span>
      }
    }
    
      
    return (
      <div className="master">
        <div class="nav-side-menu">          
          <MyModal type={this.state.modalType} callback={this.modalCallback}/>
          <div class="brand">Minha Caderneta</div>
          <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
          <div class="menu-list">
            <ul id="menu-content" class="menu-content collapse out">
              <li>
                <a href="#">
                  <i class="fa fa-dashboard fa-lg"></i> Home </a>
              </li>              
              <li>
                <a href="#" onClick={this.toggleModalDebito}>
                  <i class="fa fa-dashboard fa-lg"></i> Novo Debito 
                </a>    
              </li>
              <li>
                <a href="#" onClick={this.toggleModalPerfil}>
                  <i class="fa fa-user fa-lg"></i> Perfil </a>
              </li>
              <li>
                <a href="#" onClick={this.toggleModalSobre}>
                  <i class="fa fa-dashboard fa-lg"></i> Sobre 
                </a>    
              </li>
              <li>
                {this.state.user !== undefined}
                <a href="#" onClick={this.logout}> <i class="fa fa-users fa-lg" ></i>Logout</a>
              </li>
            </ul>
          </div>
        </div>
        <div className="Principal">
          <div className="Vector">
            <div className="Debitos">
              <a href="#"><i id="deb-text-receber">Receber</i></a>
              <a href="#"><i id="deb-text-pagar">Pagar</i></a>
            </div>            
              <div className="Clientes">
                <span onClick={this.refresh.bind(this)}>Clientes</span>
                {lista}
              </div>            
          </div>
        </div>
      </div>
    );
  }
}
//<List items={db.firestore().collection('users').get()}/>