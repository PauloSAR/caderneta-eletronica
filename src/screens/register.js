import React, { Component } from "react";

import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import "./register.css";
import Login from "./login";
import util from '../helpers/auth'

const auth = util.auth
const database = util.database

export default class Register extends Component {

  constructor(props){
    super(props);    

    this.handleSubmission = this.handleSubmission.bind(this)
    this.handleNameChange = this.handleNameChange.bind(this)
    this.handleEmailChange = this.handleEmailChange.bind(this)
    this.handlePasswdChange = this.handlePasswdChange.bind(this)

    this.state = {
      nome: '',
      sobrenome: '',
      email: '',
      senha: ''
    }
  }

  handleNameChange (e){
    if(e.target.id === 'nome'){
      this.setState({
        ...this.state,
        nome: e.target.value
      })
    } else{
      this.setState({
        ...this.state,
        sobrenome: e.target.value
      })
    }
  }

  handleEmailChange(e){
    this.setState({
      ...this.state,
      email: e.target.value
    })
  }

  handlePasswdChange(e){
    this.setState({
      ...this.state,
      senha: e.target.value
    })
  }

  async handleSubmission(e){
    e.preventDefault()
    var email = this.state.email
    var password = this.state.senha
      
    await auth.signUp({email: email, password: password}, async (e) => {
      if(e){
        window.app.dialog.showErrorBox("Erro", e.message)
      } else{
        await database.writeUserData(auth.getCurrentUser().uid, this.state.nome, this.state.email, (e) =>{
          e ? console.log(e) : console.log("sucesso")
          console.log(auth.getCurrentUser())
        })
      }
      
    })    
  }

  render() {
    return (
      <div className="master">
        <div className="foreground">
          <Route path="/login" component={Login} />
          <div id="form">
            <span id="login">Registro</span>
            <div>
              <span id="reg-text">Já tem uma conta?</span>
              <Link to="/login" id="reg-link">
                {" "}
                Clique aqui!
              </Link>
            </div>
            <form id="fields">
              <div className="form-group">
                <div className="same-row">
                  <input
                    type="text"
                    className="form-control"
                    id="nome"
                    aria-describedby="emailHelp"
                    placeholder="Nome *"
                    name="nome"
                    onChange={this.handleNameChange}
                    required
                  />
                  <input
                    type="text"
                    className="form-control"
                    id="sobrenome"
                    aria-describedby="emailHelp"
                    placeholder="Sobrenome *"
                    name="sobrenome"
                    onChange={this.handleNameChange}
                    required
                  />
                </div>
              </div>
              <div className="form-group">
                <input
                  type="email"
                  className="form-control"
                  id="exampleInputPassword1"
                  placeholder="Email *"
                  name="email"
                  onChange={this.handleEmailChange}
                  required
                />
              </div>
              <div className="form-group">
                <input
                  type="password"
                  className="form-control"
                  id="exampleInputPassword1"
                  placeholder="Senha *"
                  name="senha"
                  onChange={this.handlePasswdChange}
                  required
                />
              </div>
              <div className="form-group" id="below"></div>
              <button type="submit" className="btn btn-primary" id="submit" onClick={this.handleSubmission}>
                Registrar
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
