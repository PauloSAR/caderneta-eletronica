import React from 'react';
import logo from './logo.svg';
import './App.css';

import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import Login from './screens/login'

function App() {
  return (
    <div className="App">
      <Router>
        <Login/>
      </Router>
    </div>
  );
}

export default App;
