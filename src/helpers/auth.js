var firebase = require("firebase/app")

require("firebase/auth")
require("firebase/firestore")

var firebaseConfig = {
    apiKey: "AIzaSyDaXBZqfyxUejeG_EyU-aCs_kRlSHM1OrI",
    authDomain: "caderneta-eletronica.firebaseapp.com",
    databaseURL: "https://caderneta-eletronica.firebaseio.com",
    projectId: "caderneta-eletronica",
    storageBucket: "",
    messagingSenderId: "32417915341",
    appId: "1:32417915341:web:739e8a5da4f009a0"
};
  // Initialize Firebase
firebase.initializeApp(firebaseConfig);
var db = firebase.firestore()

const util = {
    auth: {  
        signUp(credentials, callback){
            let email = credentials.email;
            let password = credentials.password;
        
            firebase.auth().createUserWithEmailAndPassword(email, password)            
            .then(callback())
            .catch((e) => callback(e))
        },
    
        signIn(credentials, callback){
            let email = credentials.email
            let password = credentials.password
    
            firebase.auth().signInWithEmailAndPassword(email, password)
            .then(() => callback())
            .catch((e) => callback(e))
        },
        
        getCurrentUser(){
            return firebase.auth().currentUser;
        }
    },

    database: {
        writeUserData(userId, name, email, callback){
            db.collection("users").add({
                name: name,
                email: email,
                uid: userId
            })
            .then((docRef) => console.log("Document written with id: " + docRef))
            .catch((e) => callback(e))            
        },

        async getCurrentUserData(){
            let userDoc
            await db.collection("users").get().then((snap) =>
            {                
                snap.forEach((doc) =>{
                    
                    if(doc.data().uid === firebase.auth().currentUser.uid){
                        userDoc = doc
                        return
                    }
                })                
            })
            console.log(userDoc)
            return userDoc? userDoc.data() : {err: "Document doesnt exist"}
        },
        firestore: firebase.firestore
    }
}


export default util