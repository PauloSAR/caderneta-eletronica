import React from 'react';

// import { Container } from './styles';

export default function List(props) {  
  const items = props.items  
  let number = 0
  const list = items.map((item) =>{
    number = number + 1
    return (
      <li key={number.toString()}>
        <div>          
          <p>{item.valor}</p>
        </div>
      </li>
    )
  }
    
  )
  return (
    <ul>{list}</ul>
  );
}